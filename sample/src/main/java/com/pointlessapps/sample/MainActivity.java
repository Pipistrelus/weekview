package com.pointlessapps.sample;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.pointlessapps.weekview.WeekView;

import java.util.ArrayList;

public class MainActivity extends Activity {

	@Override protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_main);

		((WeekView) findViewById(R.id.weekView)).setMonthChangeListener((newYear, newMonth) -> new ArrayList<>());
	}
}
